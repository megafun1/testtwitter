package ru.ramil.azmetov.twittertest.app;
/**
 * This fragment displays first screen of the application .
 *
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import oauth.signpost.OAuthConsumer;

public class LoginFragment extends Fragment {

    private static String TAG = "ru.ramil.azmetov.twittertest.app";
    private TextView mLoginTextView;
    private Button mLoginButton;
    private Button mLogoutButton;
    private Boolean isAuthenticated = false;
    private OAuthConsumer mConsumer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onStop() {
        //saving consumer to a file
        try {
            FileOutputStream fos = getActivity().openFileOutput("SavedConsumer", Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(mConsumer);
            os.close();
        } catch (Exception ex) {
            Log.e(TAG, "Error during write to file", ex);
        }
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        //retrieving consumer from a file
        try {
            FileInputStream fisConsumer = getActivity().openFileInput("SavedConsumer");
            ObjectInputStream isConsumer = new ObjectInputStream(fisConsumer);
            OAuthConsumer consumer = (OAuthConsumer) isConsumer.readObject();
            if (consumer != null) {
                mConsumer = consumer;
                isAuthenticated = true;
                mLoginTextView.setText("Authenticated" + " " + isAuthenticated);
            }
            isConsumer.close();
            getActivity().deleteFile("SavedConsumer");

        } catch (Exception ex) {
            Log.e(TAG, "Error during read from file", ex);
        }
        // receiving from ReceiveAccessToken.class mConsumer and mProvider
        Intent intent = getActivity().getIntent();
        if (intent.getExtras() != null) {
            mConsumer = (OAuthConsumer) intent.getExtras().getSerializable(Constants.CONSUMER_TRANSACTION_KEY);
            isAuthenticated = true;
            mLoginTextView.setText("Authenticated" + " " + isAuthenticated);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLoginTextView = (TextView) view.findViewById(R.id.login_textView);
        mLoginTextView.setText("Authenticated" + " " + isAuthenticated);
        mLoginButton = (Button) view.findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if user is not authenticated run ReceiveAccessTokenActivity
                if (isAuthenticated == false) {
                    Intent i = new Intent(getActivity(), ReceiveAccessTokenActivity.class);
                    startActivity(i);
                }
                // else run TweetsListFragment with consumer and provider
                else {
                    Fragment fragment = new TweetsListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.CONSUMER_TRANSACTION_KEY, mConsumer);
                    fragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragmentContainer, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });
        mLogoutButton = (Button) view.findViewById(R.id.logout_button);
        mLogoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void logout() {
        mConsumer = null;
        isAuthenticated = false;
        mLoginTextView.setText("Authenticated" + " " + isAuthenticated);
        getActivity().deleteFile("SavedConsumer");
        CookieSyncManager.createInstance(getActivity());
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
    }
}
