package ru.ramil.azmetov.twittertest.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;

/**
 * Class receives access token from the Twitter server
 * through opening Twitter authentication page.
 * After that transmits OAuthConsumer  with access token
 * and OAuthProvider to LoginFragment.
 */
public class ReceiveAccessTokenActivity extends Activity {
    private final String TAG = getClass().getName();
    private OAuthConsumer mConsumer;
    private OAuthProvider mProvider;
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_access_token);
        mWebView = (WebView) findViewById(R.id.login_webView);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            //intercepting uri from WebView
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Uri uri = Uri.parse(url);
                if (uri.getScheme().equals(Constants.OAUTH_CALLBACK_SCHEME)) {
                    mWebView.setVisibility(View.GONE);
                    new RetrieveAccessToken(ReceiveAccessTokenActivity.this, mConsumer, mProvider).execute(uri);
                }
            }
        });
        try {
            mConsumer = new CommonsHttpOAuthConsumer(Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
            mProvider = new CommonsHttpOAuthProvider(Constants.REQUEST_URL, Constants.ACCESS_URL, Constants.AUTHORIZE_URL);
        } catch (Exception e) {
            Log.e(TAG, "Error creating consumer / provider", e);
        }
        //Before obtaining access token in asynchronous task obtain authentication URI
        new RetrieveRequestToken().execute();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public class RetrieveRequestToken extends AsyncTask<Void, Void, Void> {

        final String TAG = "ru.ramil.azmetov.twittertest.app.RetrieveRequestToken";

        /**
         * Retrieve the OAuth Request Token and open a browser to authorize the token.
         */
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Log.i(TAG, "Retrieving request token from Twitter");
                final String url = mProvider.retrieveRequestToken(mConsumer, Constants.OAUTH_CALLBACK_URL);
                mWebView.setVisibility(View.VISIBLE);
                mWebView.loadUrl(url);
            } catch (Exception e) {
                Log.e(TAG, "Error during OAUth retrieve request token", e);
            }
            return null;
        }
    }
}
