package ru.ramil.azmetov.twittertest.app;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import oauth.signpost.OAuthConsumer;

/**
 * Class displays a list of tweets.
 */
public class TweetsListFragment extends ListFragment {

    private static final int TIMER_REQUEST_CODE = 7;
    private static final int REQUEST_CODE = 1;
    private OAuthConsumer mConsumer;
    private Tweets mTweets = new Tweets();
    private TweetListFragmentAdapter mAdapter;
    private Timer mTimer = new Timer();

    @Override
    public void onResume() {
        super.onResume();
        // every minute downloading new tweets
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                // finding out id of first tweet
                Tweet firsTweet = mTweets.get(0);
                Long firstTweet_id = Long.valueOf(firsTweet.getId());
                // making request to download new tweets
                String url = Constants.HOME_TIMELINE + "?since_id=" + firstTweet_id;
                //downloading new Tweets
                new DownloadTweets(TweetsListFragment.this, mAdapter, TIMER_REQUEST_CODE)
                        .execute(url);
            }
        }, 60000, 60000);
    }

    @Override
    public void onPause() {
        mTimer.cancel();
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheDir = StorageUtils.getCacheDirectory(getActivity());
        ImageLoaderConfiguration config = new ImageLoaderConfiguration
                .Builder(getActivity()).diskCache(new UnlimitedDiscCache(cacheDir)).build();
        ImageLoader.getInstance().init(config);
        // receiving data from LoginFragment
        Bundle bundle = getArguments();
        mConsumer = (OAuthConsumer) bundle.getSerializable(Constants.CONSUMER_TRANSACTION_KEY);
        mAdapter = new TweetListFragmentAdapter(this, mTweets);
        if (mAdapter.getCount() == 0) {
            new DownloadTweets(this, mAdapter, REQUEST_CODE)
                    .execute(Constants.HOME_TIMELINE);
            setListAdapter(mAdapter);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tweets_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AdapterScrollListener listener = new AdapterScrollListener(this, mAdapter);
        getListView().setOnScrollListener(listener);
    }

    public Tweets getTweets() {
        return mTweets;
    }

    public OAuthConsumer getConsumer() {
        return mConsumer;
    }
}
