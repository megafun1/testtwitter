package ru.ramil.azmetov.twittertest.app;


import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

public class DownloadTweets extends AsyncTask<String, Void, Tweets> {

    private static final int TIMER_REQUEST_CODE = 7;
    private final String TAG = "ru.ramil.azmetov.twittertest.app.DownloadTweets";
    private TweetListFragmentAdapter mAdapter;
    private TweetsListFragment mFragment;
    private int mRequestCode;


    public DownloadTweets(TweetsListFragment fragment, TweetListFragmentAdapter adapter, int requestCode) {
        mFragment = fragment;
        mAdapter = adapter;
        mRequestCode = requestCode;
    }

    @Override
    protected Tweets doInBackground(String... params) {
        String url = params[0];
        Tweets tweets = downloadTweets(url);
        return tweets;
    }

    @Override
    protected void onPostExecute(Tweets tweets) {
        super.onPostExecute(tweets);
        if(mRequestCode == TIMER_REQUEST_CODE){
            for (int i = tweets.size()-1; i>=0;i--) {
                mFragment.getTweets().add(0,tweets.get(i));
            }
        }else {
            mFragment.getTweets().addAll(tweets);
        }
        mAdapter.notifyDataSetChanged();
        FragmentManager fm = mFragment.getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
        if (fragment.equals(mFragment) && mFragment.getListView() != null) {
            mFragment.getListView().invalidateViews();
        }
    }

    /**
     * method sends request to the Twitter server to download Tweets and
     * converts them from json format
     *
     * @param url url taken from Twitter api
     * @return array of tweets
     */
    public Tweets downloadTweets(String url) {
        Tweets tweets = new Tweets();
        try {
            ServerInteractions serverInteractions = new ServerInteractions(mFragment.getConsumer());
            // sending request and checking is request ok
            Boolean isOk = serverInteractions.sendRequest(url);
            // if ok convert tweets from json format
            if (isOk) {
                tweets = tweets.jsonToTwitter(serverInteractions.getResponseFromServer().toString());
            }
            return tweets;
        } catch (Exception e) {
            Log.e(TAG, "Error during checking authentication", e);
        }
        return tweets;
    }
}

