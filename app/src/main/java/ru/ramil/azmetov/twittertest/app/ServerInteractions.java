package ru.ramil.azmetov.twittertest.app;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import oauth.signpost.OAuthConsumer;

/**
 * Class is designed for interactions with the Twitter server
 */
public class ServerInteractions {
    private StringBuilder mResponseFromServer = new StringBuilder();
    private OAuthConsumer mConsumer;

    public ServerInteractions(OAuthConsumer consumer) {

        mConsumer = consumer;
    }

    // method returns received data from request to Twitter API in json format
    public StringBuilder getResponseFromServer() {
        return mResponseFromServer;
    }

    public void setSB(StringBuilder mSB) {
        this.mResponseFromServer = mSB;
    }

    /**
     * method sends request to the twitter api
     *
     * @param string URI resource from Twitter API
     * @return true if request successful else false
     */
    public boolean sendRequest(String string) {
        Boolean isOk = false;
        HttpGet httpGet = new HttpGet(string);
        try {
            mConsumer.sign(httpGet);
            DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpResponse response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            String reason = response.getStatusLine().getReasonPhrase();
            StringBuilder sb = new StringBuilder();

            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream inputStream = entity.getContent();
                BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                String line = null;
                while ((line = bReader.readLine()) != null) {
                    sb.append(line);
                }
                setSB(sb);
                isOk = true;
            } else {
                sb.append(reason);
                return false;
            }
        } catch (Exception ex) {
            Log.e("ServerInteractions", "Error during checking authentication", ex);
        }
        return isOk;
    }

}
