package ru.ramil.azmetov.twittertest.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TweetListFragmentAdapter extends BaseAdapter {

    private TweetsListFragment mTweetsListFragment;
    private LayoutInflater mLayoutInflater;
    private Tweets mTweets;
    private int i = 0;
    private boolean mIsDownloadingTweetsFinished = true;
    private ImageLoader mImageLoader;

    /**
     * Loading  all necessary objects. Getting the
     * inflater.
     */
    public TweetListFragmentAdapter(TweetsListFragment fragment, Tweets tweets) {

        mTweetsListFragment = fragment;
        mTweets = tweets;
        mLayoutInflater = (LayoutInflater) fragment.getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageLoader = ImageLoader.getInstance();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View item = convertView;
        final ViewHolder viewHolder;
        /**
         * If item has not been created yet, we inflate it and pass its personal
         * ViewHolder as a tag parameter. Otherwise we just get an existing
         * ViewHolder.
         */
        if (item == null) {
            item = mLayoutInflater.inflate(R.layout.list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) item.findViewById(R.id.name);
            viewHolder.text = (TextView) item.findViewById(R.id.text);
            viewHolder.avatar = (ImageView) item.findViewById(R.id.avatar);
            item.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) item.getTag();
        }
        viewHolder.name.setText(mTweets.get(position).getUser().getName());
        viewHolder.text.setText(mTweets.get(position).getText());
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheOnDisk(true).cacheInMemory(true).build();
        mImageLoader.displayImage(mTweets.get(position).getUser().getProfileImageUrl(), viewHolder.avatar, options);
        // on item click opens new TweetDetailsFragment with tweet`s details
        setOnItemClickListener(position, item);
        return item;
    }

    /**
     * Method open new TweetDetailsFragment after tweet was clicked
     *
     * @param position of the tweet
     * @param item
     */
    private void setOnItemClickListener(final int position, View item) {
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new TweetDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.SINGLE_TWEET_KEY, mTweets.get(position));
                fragment.setArguments(bundle);
                FragmentTransaction transaction = mTweetsListFragment.getActivity().
                        getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentContainer, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public int getCount() {
        return mTweets.size();
    }

    @Override
    public Object getItem(int position) {
        return mTweets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        public TextView name;
        public TextView text;
        public ImageView avatar;
    }
}



