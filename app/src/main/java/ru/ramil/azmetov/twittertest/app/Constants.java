package ru.ramil.azmetov.twittertest.app;

public class Constants {

    public static final String CONSUMER_KEY = "LVuzBeooliCBtEUVXuTZwvDEh";
    public static final String CONSUMER_SECRET = "HYiHWBW7QbD5O0ekWfs45EN1kjC2yomJKtBnISfJy95VBm7hba";

    public static final String REQUEST_URL = "https://api.twitter.com/oauth/request_token";
    public static final String ACCESS_URL = "https://api.twitter.com/oauth/access_token";
    public static final String AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize";

    public static final String OAUTH_CALLBACK_SCHEME = "x-oauthflow-twittertest";
    public static final String OAUTH_CALLBACK_HOST = "callback";
    public static final String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME + "://" + OAUTH_CALLBACK_HOST;

    public static final String VERIFY_CREDENTIALS = "https://api.twitter.com/1.1/account/verify_credentials.json";
    public static final String HOME_TIMELINE = "https://api.twitter.com/1.1/statuses/home_timeline.json";

    public static final String CONSUMER_TRANSACTION_KEY = "ru.ramil.azmetov.twittertest.app.consumer_key";
    public static final String PROVIDER_TRANSACTION_KEY = "ru.ramil.azmetov.twittertest.app.provider_key";
    public static final String SINGLE_TWEET_KEY = "ru.ramil.azmetov.twittertest.app.single_tweet_key";

}

