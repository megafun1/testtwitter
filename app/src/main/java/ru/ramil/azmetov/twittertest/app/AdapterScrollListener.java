package ru.ramil.azmetov.twittertest.app;

import android.widget.AbsListView;

/**
 * Listener that allows to download new Tweets when screen reached tenth bottom item
 */
public class AdapterScrollListener implements AbsListView.OnScrollListener {

    private static final int REQUEST_CODE = 5;
    private TweetsListFragment mFragment;
    private int mCount = 0;
    private TweetListFragmentAdapter mAdapter;

    public AdapterScrollListener(TweetsListFragment fragment, TweetListFragmentAdapter adapter) {
        mFragment = fragment;
        mAdapter = adapter;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount == totalItemCount - 10 && totalItemCount != 0
                && mCount != mFragment.getTweets().size()) {
            mCount = mFragment.getTweets().size();
            // getting last tweet
            int i = mFragment.getTweets().size();
            Tweet tweet = mFragment.getTweets().get(i - 1);
            // getting last tweet`s id to use Twitter`s api parameter called max_id
            String id = tweet.getId();
            Long intId = Long.valueOf(id) - 1;
            //downloading new tweets which id is less than our last tweet`s id
            new DownloadTweets(mFragment, mAdapter, REQUEST_CODE).execute(Constants.HOME_TIMELINE + "?max_id=" + intId + "&count=15");
        }
    }
}
