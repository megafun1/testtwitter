package ru.ramil.azmetov.twittertest.app;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;

public class Tweets extends ArrayList<Tweet> {

    public Tweets jsonToTwitter(String result) {
        Tweets tweets = new Tweets();
        Tweet tweet;
        if (result != null && result.length() > 0) {
            try {
                Gson gson = new Gson();
                JsonParser parser = new JsonParser();
                JsonArray jArray = parser.parse(result).getAsJsonArray();
                for (JsonElement jsonElement : jArray) {
                    tweet = gson.fromJson(jsonElement, Tweet.class);
                    tweets.add(tweet);

                }
            } catch (Exception ex) {
                Log.e("Tweet", "Json error", ex);
            }
        }
        return tweets;
    }
}
