package ru.ramil.azmetov.twittertest.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Class displays tweet details.
 */
public class TweetDetailsFragment extends Fragment {
    Tweet mTweet;
    TextView mNameTextView;
    TextView mInReplyToStatusIdTextView;
    TextView mInReplyToUserIdTextView;
    TextView mInReplyToScreenNameTextView;
    TextView mScreenNameTextView;
    ImageView mAvatar;
    TextView mText;
    TextView mIdTextView;
    TextView mDateCreatedTextView;

    private void loadViews(View view) {
        mNameTextView = (TextView) view.findViewById(R.id.name);
        mScreenNameTextView = (TextView) view.findViewById(R.id.screen_name);
        mAvatar = (ImageView) view.findViewById(R.id.avatar);
        mInReplyToScreenNameTextView = (TextView) view.findViewById(R.id.in_reply_to_screen_name);
        mInReplyToStatusIdTextView = (TextView) view.findViewById(R.id.in_reply_to_status_id);
        mInReplyToUserIdTextView = (TextView) view.findViewById(R.id.in_reply_to_user_id);
        mText = (TextView) view.findViewById(R.id.text);
        mIdTextView = (TextView) view.findViewById(R.id.textView_id);
        mDateCreatedTextView = (TextView) view.findViewById(R.id.date_created_textView);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mTweet = (Tweet) bundle.getSerializable(Constants.SINGLE_TWEET_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tweet_details, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadViews(view);
        if (mInReplyToScreenNameTextView.equals(null)) mInReplyToScreenNameTextView.setText("");
        else mInReplyToScreenNameTextView.setText(mTweet.getInReplyToScreenName());
        if (mInReplyToStatusIdTextView.equals(null)) mInReplyToStatusIdTextView.setText("");
        else mInReplyToStatusIdTextView.setText(mTweet.getInReplyToScreenName());
        if (mInReplyToUserIdTextView.equals(null)) mInReplyToUserIdTextView.setText("");
        else mInReplyToUserIdTextView.setText(mTweet.getInReplyToScreenName());
        mNameTextView.setText(mTweet.getUser().getName());
        mScreenNameTextView.setText(mTweet.getUser().getScreenName());
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(mTweet.getUser().getProfileImageUrl(), mAvatar);
        mText.setText(mTweet.getText());
        mIdTextView.setText("Id = " + mTweet.getId());
        mDateCreatedTextView.setText("Created at:" + mTweet.getDateCreated());
    }
}
