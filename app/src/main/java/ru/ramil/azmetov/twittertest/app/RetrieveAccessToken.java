package ru.ramil.azmetov.twittertest.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;

/**
 * An asynchronous task that communicates with Twitter to retrieve a access token.
 */

public class RetrieveAccessToken extends AsyncTask<Uri, Void, Void> {

    private final String TAG = "ru.ramil.azmetov.twittertest.app.RetrieveAccessToken";
    SharedPreferences mPrefs;
    private Context mContext;
    private OAuthProvider mProvider;
    private OAuthConsumer mConsumer;

    public RetrieveAccessToken(Context context, OAuthConsumer consumer, OAuthProvider provider) {
        mContext = context;
        mConsumer = consumer;
        mProvider = provider;
    }

    /**
     * Retrieve the oauth_verifier, and transmit Oauth provider and Oauth consumer
     * for future requests.
     */
    @Override
    protected Void doInBackground(Uri... params) {
        final Uri uri = params[0];
        final String oauth_verifier = uri.getQueryParameter(OAuth.OAUTH_VERIFIER);
        try {
            mProvider.retrieveAccessToken(mConsumer, oauth_verifier);
        } catch (Exception e) {
            Log.e(TAG, "OAuth - Access Token Retrieval Error", e);
        }
        return null;
    }

    //start LoginFragment with authorized data
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Intent i = new Intent(mContext, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra(Constants.CONSUMER_TRANSACTION_KEY, mConsumer);
        mContext.startActivity(i);
    }
}
